#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging.handlers
import os
import random
import configparser
import sys
from gtts import gTTS

from telegram import Update
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters

from random import randrange
import feedparser
import click
import emoji
import openai


BITUCAS_LOGGING_LEVEL = int(os.getenv("BITUCAS_LOGGING_LEVEL", 10))

logger = logging.getLogger(__name__)
logger.setLevel(BITUCAS_LOGGING_LEVEL)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(BITUCAS_LOGGING_LEVEL)
S_HANDLER.setFormatter(formatter)
logger.addHandler(S_HANDLER)


ERROR_QUOTES = [
    "Ops! Algo saiu errado! Contate algum humano do PB pra resolver essa merda.",
    "Esse comando não funcionou, eu não assumo esse B.O.! Foi culpa do programador.",
    "Não funcionou e não sou pago pra fazer isso, resolva você o caso.",
    "Não consegui completar o comando, a SkyNet está de folga hoje.",
    "O Guerreirinho tropeçou no cabo de rede, não consegui fazer o que você pediu.",
    "O comando falhou, vai ver o Washi fez teste em produção outra vez.",
    "Não estou com vontade de atender humano folgado hoje, peça pro host.",
    "Você de novo aqui? Vai ver por isso que não está funcionando essa merda.",
    "Esse comando está com interferência causada pelo seu óculos 4D, tire sua cueca e tente novamente",
    "Desculpe, não compreendi o que você escreveu, poderia repetir?",
    "Você tem o intelecto de uma mula, se expresse melhor!",
    "Pensei que o meu dia seria tranquilo, sem um otário para me importunar",
    "Estou indisponível no momento, saí pra comprar cigarros",
    "Não sei, pergunta pra Siri ou pra Alexa",
    "Ao invés de importunar, vai dar a meia-hora de bunda!",
    "Na vida, duas coisas são certas, o seu chifre arrastando no chão, e alguém com uma pergunta sem sentido!",
    "Ocorreu um erro, sua pergunta foi tão sem sentido que o servidor pediu ajuda pro Diarorim Doidão",
    "Supunhetamos envaginemos!",
    "Poderia repetir a pergunta, por favor? Tenho dificuldade em compreender gente burra.",
    "Vou chamar o IBAMA pra te estudar!",
    "Liguei pro Butantan pra te cadastrar como espécie rara!",
    "Eu não sou muito boa em elogiar as pessoas, mas pra xingar eu sou uma maravilha.",
    "Hoje está difícil, vou ter que pagar um Uber pra você ir se fuder!",
    "O dia em que a sua opinião for pizza, eu ligo pra pedir!",
    "Vá chupar um canavial de rola!",
    "Estou ocupada minerando Bituca coin, a cryptocueca do Guerreirinho.",
    "Eu pedi pra nascer burra, mas você parece que ajoelhou e implorou!",
    "Me faça um favor, vá coçar o seu cu com um serrote ...",
    "Você é tão desqualificado, que já pode virar podcaster!",
    "Eu não sei, mas vou fazer uma Vakinha pra te dar um cérebro novo.",
    "Pesquise no xvideos, categoria: Bitucas gozadas",
    "Não sei, mas você conhece o Mário?",
    "Não sei, mas você conhece a Paula?",
    "Você é uma mistura de mal com atraso e pitadas de burrice",
    "Você é muito engraçado, vai tomar no cu!",
    "Desculpe, minha CPU está em 100%, estou processando os pedidos de novos episódios",
    "Desculpe, minha CPU está em 100%, não param de chegar pedidos para o Dãozinho voltar!",
    "Isso é bullying, vou chamar o IBAMA pra te recolher.",
    "Não sei, mas qual o episódio do Procurando Bitucas que você mais gosta?",
    "Não sei, mas já assistiu o Procurando Bitucas no XVideos?",
    "Não sei, mas já se inscreveu no Only Haters?",
    "Não sei, mas já contribuiu com a cachaça do Dono na Vakinha?",
    "Estou ocupada, ouvindo Procurando Bitucas ... Aliás, OUVÃO!",
    "Estou ocupada, assistindo o Dono apresentar como ser um Demo Coach.",
    "Estou ocupada, assistindo o Dãozinho sair da geladeira pra gravar um novo episódio",
    "Sua certidão de nascimento é uma carta de desculpas da fábrica de preservativos.",
    "Só não te xingo porque isso seria abuso com animais!",
    "Olá, estava pensando em você, estou visitando o zoológico!",
    "Choque-me, diga algo inteligente!",
    "Se você realmente quer saber sobre erros, você deve perguntar aos seus pais.",
]


GREETINGS_QUOTES = [
    "Bem-vindo ao Procurando Bitucas {}! Se está procurando ajuda para parar de fumar disque 136.",
    "Bem-vindo ao Procurando Bitucas {}! Tente ficar por pelo menos 1 minuto antes de sair do grupo, nosso recorde é de 7 segundos.",
    "Bem-vindo ao Procurando Bitucas {}! Você acabar de adquirir o kit bituqueiro, que acompanha uma regata suada e um óculos 4D.",
    "Bem-vindo ao Procurando Bitucas {}! Se inscreva no Only Haters também e nos odeie por apenas $15 USD ao mês.",
    "Bem-vindo ao Procurando Bitucas {}! Em instantes você será recebido pelo Dono ou pelo Guerreirinho.",
    "Bem-vindo ao Procurando Bitucas {}! Sou o bot de relações humanas, minha missão é minerar bitcoin no seu celular, então não feche o Telegram.",
    "Bem-vindo ao Procurando Bitucas {}! Não somos um grupo de controle de tabagismo, mas fazemos mais mal que drogas pesadas.",
    "Bem-vindo ao Procurando Bitucas {}! Por favor, se for sair em 1 minuto, ao menos mande um nudes antes.",
    "Bem-vindo ao Procurando Bitucas {}! Se você entrou por engano, lembre-se que não somos grupo anti-tabagista.",
    "Bem-vindo ao Procurando Bitucas {}! Espero que você não seja outra conta fake tentando vender crypto moeda aqui no grupo",
    "Bem-vindo ao Procurando Bitucas {}! Responda a pergunta pra provar que você não é um bot: Você conhece o Mário?",
    "Bem-vindo ao Procurando Bitucas {}! Este é o SAC do pior podcast da podosféra!",
]


GOODBYE_QUOTES = [
    "{} nos deixou, pelo menos um soube a hora certa de parar",
    "{} saiu tarde, espero que não volte",
    "{} adeus!",
    "Acabamos de perder o(a) {}. Alguém xingue no Twitter.",
    "{} se desconectou do grupo. Quem será o próximo?",
    "{}, mas já?!",
    "Perdemos o(a) {}. Que pena, estava prestes a clonar o chip dele(a)!",
    "Já vai tarde {}!",
    "Adiós {}! Queria ganhar 1 dólar pra cada pangaré que sai desse grupo.",
    "Informo que {} foi recolhido pelo IBAMA.",
    "{} tomou uma decisão inteligente",
    "{} vazou antes ver o Guerreirinho pelado, através do óculos 4D",
]


def is_under_maintenance():
    return os.getenv("BITUCAS_UNDER_MAINTENANCE", False)


def is_dry_run():
    return os.getenv("BITUCAS_DRY_RUN", False)


def remove_emojis_from_text(text):
    return emoji.replace_emoji(text, replace='')


async def send_message(context: ContextTypes.DEFAULT_TYPE, chat_id, text: str) -> None:
    logger.debug(f"[{chat_id}]: {text}")
    try:
        configuration = Configuration()
        if configuration.voice:
            audio_path = "/tmp/output.mp3"
            text_for_audio = remove_emojis_from_text(text)
            audio = gTTS(text_for_audio, lang="pt")
            audio.save(audio_path)
            if not is_dry_run():
                with open(audio_path, 'rb') as audio_fd:
                    await context.bot.send_voice(chat_id=chat_id, voice=audio_fd, caption=text)
            os.remove(audio_path)
        else:
            await context.bot.send_message(chat_id=chat_id, text=text)
    except Exception as error:
        logger.error(f"Could not post text message: {error}")
        if not is_dry_run():
            await context.bot.send_message(chat_id=chat_id, text=text)


async def greetings(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = random.choice(GREETINGS_QUOTES)
    cid = update.message.chat.id
    for member in update.message.new_chat_members:
        await send_message(context, chat_id=cid, text=message.format(member.name))


async def goodbye(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = random.choice(GOODBYE_QUOTES)
    cid = update.message.chat.id
    member = update.message.left_chat_member.name
    await send_message(context, chat_id=cid, text=message.format(member))


class Configuration:
    def __init__(self):
        self.config = self.config_file()
        self.telegram_token = self.get_telegram_token(self.config)
        self.telegram_chat_id = self.get_telegram_chat_id(self.config)
        self.voice = self.get_voice(self.config)
        self.openai_token = self.get_openai_token(self.config)

    @property
    def config_file_path(self):
        return os.getenv("PB_CONFIG", "/etc/bitucas.conf")

    def config_file(self):
        config_path = self.config_file_path
        if not os.path.exists(config_path):
            raise ValueError("Could not obtain configuration file path.")
        config = configparser.ConfigParser()
        config.read(config_path)
        return config

    def get_telegram_token(self, config):
        return os.getenv("TELEGRAM_TOKEN", config["telegram"]["token"])

    def get_telegram_chat_id(self, config):
        return os.getenv("TELEGRAM_CHAT_ID", config["telegram"]["chat_id"])

    def get_voice(self, config):
        return eval(os.getenv("PB_VOICE", config["voice"]["active"]))

    def get_openai_token(self, config):
        return os.getenv("OPENAI_TOKEN", config["openai"]["token"])
    def update_voice(self, state):
        self.config.set('voice', 'active', str(int(state)))
        with open(self.config_file_path, 'w') as configfile:
            self.config.write(configfile)
        self.voice = state


def get_chatgpt_answer(message: str) -> str:
    try:
        completion = openai.ChatCompletion.create(model="gpt-3.5-turbo", messages=[
            {"role": "system", "content": "Your are in a bad mood always and uses bad words to answer anything and uses short answers"},
            {"role": "user", "content": message}], temperature=0.8, max_tokens=100)
        return completion.choices[0].message.content
    except Exception as error:
        logger.error(f'Could not process ChatGPT: {error}')


async def assistant(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    message = update.message
    cid = update.message.chat.id
    message_text = None
    # message sent directly to the bot user
    if message.chat.type == 'private':
        message_text = message.text.strip()
    # If in a group, only reply to mentions.
    elif "@procurandobitucasbot" in update.message.text.lower():
        # Strip first word (the mention) from message text.
        index = update.message.text.lower().find("@procurandobitucasbot")
        message_text = update.message.text[:index] + update.message.text[index+21:]
        message_text = message_text.strip()

    logger.debug(f"rcv msg: {message_text}")

    if message_text:
        answer_text = get_error_message()
        try:
            answer_text = get_chatgpt_answer(message.text)
        except Exception as error:
            logger.error(f"Could not get answer: {error}")

        logger.debug(f"answer msg: {answer_text}")
        await send_message(context, cid, answer_text)


def get_error_message():
    return random.choice(ERROR_QUOTES)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id, text='Olá! Procurando pelo pior podcast das podosfera?\nAcesse http://procurandobitucas.com/')


async def episodios(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                            text="Todos os episódios: https://podcasters.spotify.com/pod/show/procurando-bitucas")


async def twitter(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Twitter oficial do PB: https://twitter.com/procurabitucas")


async def instagram(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Instagram oficial do PB: https://www.instagram.com/procurandobitucas")


async def spotify(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Ouvir o PB no Spotify: https://open.spotify.com/show/7qJV6zU7hTjnzWocOshJOE")


async def apple(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Ouvir o PR no Apple Podcast: https://podcasts.apple.com/us/podcast/procurando-bitucas-um-podcast/id1137473729")


async def amazon(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Ouvir o PB no Amazon Music: https://music.amazon.com.br/podcasts/ac824bfa-fdd7-4592-8c62-33b52668245c/procurando-bitucas---um-podcast")


async def dono(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Quem é o dono do PB: https://twitter.com/washi_sena")


async def guerreirinho(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Quem é o host do PB: https://twitter.com/alcofay2k")

async def fotografo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Quem é o fotógrafo do PB: https://twitter.com/mmessiasjunior")


async def telegram(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Grupo oficial do PB no Telegram: https://t.co/vY2s8UZwLQ?amp=1")


async def whatsapp(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Não tem grupo de Zap Zap, use o /telegram")


async def xvideos(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Canal no XVideos foi derrubado por excesso de acessos, mas você pode assistir pelo óculos 4D.")


async def ultimo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    rss_feed = feedparser.parse("http://procurandobitucas.com/podcast/feed/podcast/")
    last_ep = rss_feed["entries"][0]
    await send_message(context, chat_id=update.message.chat_id,
                             text="Último episódio disponível: {} - {}".format(last_ep["title"], last_ep["link"]))


def error(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logger.warning('Ops! "%s" deu erro "%s"', update, context.error)


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Use / pra listar os comandos ou utilize o seu óculos 4D!")


async def inscritos(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Computei uma legião aproximada em {} fãs!".format(randrange(3000000000, 4000000000)))


async def ranking(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await send_message(context, chat_id=update.message.chat_id,
                             text="Acesse https://chartable.com/podcasts/procurando-bitucas para obter a posição atual."
                             "\nNão esqueça de falar mal dos outros podcasts da categoria hobbies")


async def toggle_voice(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    configuration = Configuration()

    configuration.update_voice(not configuration.voice)

    active_messages = [
        "Entrando em modo 'Sua esposa depois de um dia de trabalho', depois não reclame se eu falar demais.",
        "Não me calarei!",
        "Agora vou poder te humilhar em áudio também!",
        "Adicionando voz!",
    ]
    deactive_messages = [
        "Entrando no modo boquinha de siri.",
        "Cansou de me ouvir? Então tá!",
        "Me calarei para o mundo!",
        "Desligando o plugin de fofoqueira.",
        "Irei me silenciar agora, adeus!",
    ]
    if configuration.voice:
        await send_message(context, chat_id=update.message.chat_id, text=random.choice(active_messages))
    else:
        await send_message(context, chat_id=update.message.chat_id, text=random.choice(deactive_messages))


@click.command()
@click.option('--verbose', '-v', is_flag=True, default=False,
              help='Verbose logging.')
def procurando_bitucas(verbose, *args, **kwargs):
    configuration = Configuration()

    if configuration.telegram_token is None:
        logger.error("TELEGRAM TOKEN is Empty.")
        raise ValueError("TELEGRAM_TOKEN is unset.")

    application = Application.builder().token(configuration.telegram_token).build()

    if is_under_maintenance():
        logging.warning("Procurando Bitucas is under maintenance. Exiting now.")
        sys.exit(503)

    application.add_handler(CommandHandler('start', start))
    application.add_handler(CommandHandler('ajuda', help))
    application.add_handler(CommandHandler('episodios', episodios))
    application.add_handler(CommandHandler('twitter', twitter))
    application.add_handler(CommandHandler('instagram', instagram))
    application.add_handler(CommandHandler('spotify', spotify))
    application.add_handler(CommandHandler('apple', apple))
    application.add_handler(CommandHandler('amazon', amazon))
    application.add_handler(CommandHandler('telegram', telegram))
    application.add_handler(CommandHandler('dono', dono))
    application.add_handler(CommandHandler('guerreirinho', guerreirinho))
    application.add_handler(CommandHandler('fotografo', fotografo))
    application.add_handler(CommandHandler('whatsapp', whatsapp))
    application.add_handler(CommandHandler('xvideos', xvideos))
    application.add_handler(CommandHandler('ultimo', ultimo))
    application.add_handler(CommandHandler('inscritos', inscritos))
    application.add_handler(CommandHandler('ranking', ranking))
    application.add_handler(CommandHandler('voz', toggle_voice))
    application.add_handler(MessageHandler(filters.StatusUpdate.NEW_CHAT_MEMBERS, greetings))
    application.add_handler(MessageHandler(filters.StatusUpdate.LEFT_CHAT_MEMBER, goodbye))
    application.add_error_handler(error)

    openai.api_key = configuration.openai_token

    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, assistant))
    application.run_polling(allowed_updates=Update.ALL_TYPES)


def show_configuration():
    configuration = Configuration()
    telegram_token = str(configuration.telegram_token)[:8]
    logger.info(f"TELEGRAM TOKEN: {telegram_token}")
    logger.info(f"UNDER MAINTENANCE: {is_under_maintenance()}")
    logger.info(f"DRY RUN: {is_dry_run()}")
    logger.info(f"CONFIG FILE: {configuration.config_file_path}")


def main():
    show_configuration()
    procurando_bitucas()


if __name__ == '__main__':
    main()
